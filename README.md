# A personal try of a-maze-ing #

This game is inspired (and a try to translate from perl) from https://github.polettix.it/ETOOBUSY/2020/01/07/a-maze-ing-2/
First version is using Symfony/Console but next will be with plain PHP only.

## Installation ##
`composer install`

## Run ##
`./a-maze-ing-3` or `./a-maze-ing-3 <width> <height>`
