<?php
declare(strict_types=1);

namespace App\Command;

use App\Game;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Cursor;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\StreamableInputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class Maze
 */
class MazeCommand extends Command
{
    protected static $defaultName = 'maze:run';

    public function configure()
    {
        $this
            ->addArgument('rows', InputArgument::OPTIONAL, 'Number of rows', 29)
            ->addArgument('cols', InputArgument::OPTIONAL, 'Number of columns', 136);
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        if ($input instanceof StreamableInputInterface && $stream = $input->getStream()) {
            $inputStream = $stream;
        } else {
            $inputStream = STDIN;
        }

        stream_set_blocking($inputStream, true);
        $sttyMode = shell_exec('stty -g');
        shell_exec('stty -icanon -echo');

        $cursor = new Cursor($output);
        $cursor->hide();

        // === Styling
        $output->getFormatter()->setStyle('background', new OutputFormatterStyle('white'));
        $output->getFormatter()->setStyle('hero', new OutputFormatterStyle('red'));
        $output->getFormatter()->setStyle('red', new OutputFormatterStyle('red'));
        $output->getFormatter()->setStyle('green', new OutputFormatterStyle('green'));
        $output->getFormatter()->setStyle('exit', new OutputFormatterStyle('green'));

        // === GAME

        $game = new Game((int)$input->getArgument('cols'), (int)$input->getArgument('rows'));
        $game->setRenderer($output);
        $game->setCursor($cursor);
        $game->run($inputStream);

        // === return to normal state
        $cursor->show();

        stream_set_blocking($inputStream, false);
        shell_exec(sprintf('stty %s', $sttyMode));

        return Command::SUCCESS;
    }
}
