<?php
declare(strict_types=1);

namespace App;

/**
 * Class Coordinate
 */
class Coordinate
{
    public int $x;
    public int $y;

    public function __construct(int $x, int $y)
    {
        $this->x = $x;
        $this->y = $y;
    }

    public function equals(Coordinate $target): bool
    {
        return $target->x === $this->x && $target->y === $this->y;
    }
}
