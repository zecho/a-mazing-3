<?php
declare(strict_types=1);

namespace App;

use Symfony\Component\Console\Cursor;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class Game
 */
class Game
{
    protected const KEY_UP = '1b5b41';
    protected const KEY_DOWN = '1b5b42';
    protected const KEY_RIGHT = '1b5b43';
    protected const KEY_LEFT = '1b5b44';
    protected const KEY_Q = '71';

    /**
     * @var Maze
     */
    protected Maze $maze;

    protected ?Coordinate $exit = null;

    /**
     * Easter egg
     * @var int
     */
    protected int $invalidMoves = 0;

    private int $moves = 0;

    private Coordinate $hero;

    private ?OutputInterface $renderer = null;

    private ?Cursor $cursor = null;

    public function __construct(int $cols, int $rows)
    {
        $this->maze = new Maze($cols, $rows);
    }

    public function setRenderer(OutputInterface $renderer): void
    {
        $this->renderer = $renderer;
    }

    public function setCursor(Cursor $cursor): void
    {
        $this->cursor = $cursor;
    }

    public function run($inputStream): void
    {
        $this->maze->init();
        $this->setHero(new Coordinate(1, 1));
        $this->setExit(new Coordinate($this->maze->width - 2, $this->maze->height - 1));
        $this->render();

        while ($key = bin2hex(fread($inputStream, 3))) {
            switch ($key) {
                case self::KEY_UP:
                    $this->tryMove(0, -1);
                    break;
                case self::KEY_DOWN:
                    $this->tryMove(0, 1);
                    break;
                case self::KEY_LEFT:
                    $this->tryMove(-1, 0);
                    break;
                case self::KEY_RIGHT:
                    $this->tryMove(1, 0);
                    break;
                case self::KEY_Q:
                    return;
                default:
                    echo $key;
                    break;
            }

            if ($this->isExit($this->hero)) {
                $this->cursor->moveToPosition(1, $this->maze->height+1);
                $this->renderer->writeln('You found the exit!');
                $this->renderer->writeln(sprintf('with only %d moves', $this->moves));

                return;
            }
        }
    }

    public function render(?Coordinate $coordinate = null): void
    {
        if ($coordinate instanceof Coordinate) {
            $this->cursor->moveToPosition($coordinate->x+1, $coordinate->y);
            $this->renderer->write($this->maze->getCell($coordinate));
            return;
        }

        //Full render
        $this->cursor->clearScreen();
        $this->cursor->moveToPosition(0, 0);

        foreach ($this->maze as $mazeRow) {
            $this->renderer->writeln(implode('', $mazeRow));
        }
    }

    public function tryMove(int $deltaX, int $deltaY): void
    {
        $newCell = new Coordinate($this->hero->x + $deltaX, $this->hero->y + $deltaY);

        if ($deltaX === 0) {
            $direction = $deltaY > 0 ? 'down' : 'up';
        } else {
            $direction = $deltaX > 0 ? 'right' : 'left';
        }

        if ($this->maze->canMove($newCell)) {
            $this->maze->addVisited($this->hero);
            $this->render($this->hero);
            $this->setHero($newCell, $direction);
            $this->addMove();
        } else {
            $this->addMove(false);
        }

        $this->render($newCell);
    }

    public function setExit(Coordinate $coordinate): void
    {
        $this->exit = $coordinate;
        $this->maze->setCell($coordinate, Maze::EXIT_DOWN, 'exit');
    }

    public function setHero(Coordinate $coordinate, $direction = 'none'): void
    {
        $this->hero = $coordinate;
        $this->maze->setCell($coordinate, Maze::$directions[$direction], 'hero');
    }

    private function isExit(Coordinate $coordinate): bool
    {
        return $this->exit->equals($coordinate);
    }

    private function addMove(bool $valid = true): void
    {
        if (!$valid) {
            $this->invalidMoves++;

            if ($this->invalidMoves >= 3) {
                $this->displayInfo();
            }
        } else {
            $this->hideInfo();
            $this->moves ++;
            $this->invalidMoves = 0;
        }
    }

    private function hideInfo(): void
    {
        $this->cursor->moveToPosition(0, $this->maze->height + 1);
        $this->renderer->writeln(str_pad(' ', 100));
    }

    private function displayInfo(): void
    {
        $this->cursor->moveToPosition(0, $this->maze->height + 1);
        $this->renderer->write('What are you trying to achieve? A headache?');
    }
}
