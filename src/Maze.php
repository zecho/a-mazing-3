<?php
declare(strict_types=1);

namespace App;

/**
 * Class Maze
 */
class Maze implements \Iterator
{
    public const EMPTY = ' ';
    public const BRICK = 'o';
    public const VISITED = '·';

    public const HERO_STILL = '◆';
    public const HERO_DOWN = '↓';
    public const HERO_LEFT = '←';
    public const HERO_RIGHT = '→';
    public const HERO_UP = '↑';

    //up|left|down|right
    public const WALL_RIGHT = 1;
    public const WALL_DOWN = 2;
    public const WALL_DOWN_RIGHT = 3;
    public const WALL_LEFT = 4;
    public const WALL_HORIZONTAL = 5;
    public const WALL_DOWN_LEFT = 6;
    public const WALL_HORIZONTAL_DOWN = 7;
    public const WALL_UP = 8;
    public const WALL_UP_RIGHT = 9;
    public const WALL_VERTICAL = 10;
    public const WALL_VERTICAL_RIGHT = 11;
    public const WALL_UP_LEFT = 12;
    public const WALL_HORIZONTAL_UP = 13;
    public const WALL_VERTICAL_LEFT = 14;
    public const WALL_VERTICAL_HORIZONTAL = 15;

    public static array $walls = [
        self::WALL_RIGHT => '╶',
        self::WALL_DOWN => '╷',
        self::WALL_DOWN_RIGHT => '┌',
        self::WALL_LEFT => '╴',
        self::WALL_HORIZONTAL => '─',
        self::WALL_DOWN_LEFT => '┐',
        self::WALL_HORIZONTAL_DOWN => '┬',
        self::WALL_UP => '╵',
        self::WALL_UP_RIGHT => '└',
        self::WALL_VERTICAL => '│',
        self::WALL_VERTICAL_RIGHT => '├',
        self::WALL_UP_LEFT => '┘',
        self::WALL_HORIZONTAL_UP => '┴',
        self::WALL_VERTICAL_LEFT => '┤',
        self::WALL_VERTICAL_HORIZONTAL => '┼',
    ];


    public static array $directions = [
        'none' => self::HERO_STILL,
        'left' => self::HERO_LEFT,
        'right' => self::HERO_RIGHT,
        'up' => self::HERO_UP,
        'down' => self::HERO_DOWN,
    ];

    public const EXIT_DOWN = '↓';

    /**
     * @var array
     */
    private array $board = [];

    /**
     * @var int
     */
    public int $width;

    /**
     * @var int
     */
    public int $height;

    private array $visited = [];

    public function __construct(int $cols, int $rows)
    {
        $rows -= 4; //For ending message

        while (0 === $rows % 2) {
            $rows--;
        }
        while (0 === $cols % 2) {
            $cols--;
        }

        $this->height = $rows;
        $this->width = $cols;
    }

    private static function updateWalls(array $maze): array
    {
        $maze = array_map(static function ($row, $rowIndex) use ($maze) {
            return array_map(static function ($content, $colIndex) use ($rowIndex, $maze) {
                if ($content === self::EMPTY) {
                    return $content;
                }

                return static::$walls[
                    (($maze[$rowIndex - 1][$colIndex] ?? null) === static::BRICK) * 8 +
                    (($maze[$rowIndex][$colIndex - 1] ?? null) === static::BRICK) * 4 +
                    (($maze[$rowIndex + 1][$colIndex] ?? null) === static::BRICK) * 2 +
                    (($maze[$rowIndex][$colIndex + 1] ?? null) === static::BRICK) * 1
                ];
            }, $row, array_keys($row));
        }, $maze, array_keys($maze));

        return $maze;
    }

    public function init(): void
    {
        $height = $this->height - 2;
        $width = $this->width - 2;

        $maze = [];
        for ($r = 0; $r < $height; $r++) {
            $maze[$r] = array_fill(0, $width, static::BRICK);
        }
        $maze[0][0] = static::EMPTY;

        $row = 0;
        $col = 0;

        while ($row < $height) {
            if ($maze[$row][$col] === static::BRICK) {
                $path = self::randomWalk($maze, $row, $col);
                $path = self::pathLoopErasure($path);

                $pr = null;
                $pc = null;
                foreach ($path as $value) {
                    [$r, $c] = [$value['row'], $value['col']];
                    $maze[$r][$c] = static::EMPTY;
                    if ($pr !== null) {
                        $maze[($r + $pr) / 2][($c + $pc) / 2] = static::EMPTY;
                    }
                    [$pr, $pc] = [$r, $c];
                }
            }

            $col += 2;
            if ($col > $width) {
                [$row, $col] = [$row + 2, 0];
            }
        }

        $hWall = array_fill(0, $this->width, static::BRICK);

        //outer walls
        $maze = array_merge(
            [$hWall],
            array_map(static function ($row) {
                return array_merge([static::BRICK], $row, [static::BRICK]);
            }, $maze),
            [$hWall]
        );

        $maze = self::updateWalls($maze);

        $this->board = $maze;
    }

    private static function randomWalk(array $maze, int $row, int $col): array
    {
        $result = [];
        $moves = [[-2, 0], [0, 2], [2, 0], [0, -2]];
        $maxRow = count(array_column($maze, 0)); //TODO: Refactor from Maze
        $maxCol = count($maze[0]); //TODO: Refactor from Maze

        while ('necessary') {
            $result[] = [
                'row' => $row,
                'col' => $col,
                'id' => "$row-$col",
            ];

            if ($maze[$row][$col] === static::EMPTY) {
                break;
            }

            $move = $moves[array_rand($moves)];
            [$cr, $cc] = [$row + $move[0], $col + $move[1]];

            if ($cr < 0 || $cr > $maxRow || $cc < 0 || $cc > $maxCol) {
                continue;
            }

            [$row, $col] = [$cr, $cc];
        }

        return $result;
    }

    private static function pathLoopErasure(array $inputPath): array
    {
        $outputPath = [];
        $inputPath = array_values($inputPath);

        $i = -1;
        $n = count($inputPath);

        while (++$i < $n) {
            $j = $i;
            while (++$j < $n) {
                if ($inputPath[$i]['id'] === $inputPath[$j]['id']) {
                    $i = $j;
                }
            }

            $outputPath[] = $inputPath[$i];
        }

        return $outputPath;
    }

    /**
     * @param Coordinate $coordinate
     * @return string
     */
    public function getCell(Coordinate $coordinate): string
    {
        return $this->board[$coordinate->y][$coordinate->x];
    }

    /**
     * @param Coordinate $coordinate
     * @param string $content
     * @param string|null $style
     */
    public function setCell(Coordinate $coordinate, string $content, string $style = null): void
    {
        $content = $style ? sprintf("<$style>%s</$style>", $content) : $content;
        $this->board[$coordinate->y][$coordinate->x] = $content;
    }

    public function addVisited(Coordinate $coordinate): void
    {
        $this->visited[$coordinate->y][$coordinate->x] = ($this->visited[$coordinate->y][$coordinate->x] ?? 0) + 1;
        $this->setCell($coordinate, self::VISITED, $this->visited[$coordinate->y][$coordinate->x] > 1 ? 'red' : 'green');
    }

    public function canMove(Coordinate $coordinate): bool
    {
        return !in_array($this->board[$coordinate->y][$coordinate->x], self::$walls, true);
    }

    public function current()
    {
        return current($this->board);
    }

    public function next()
    {
        return next($this->board);
    }

    public function key()
    {
        return key($this->board);
    }

    public function valid(): bool
    {
        $key = key($this->board);

        return ($key !== null && $key !== false);
    }

    public function rewind()
    {
        reset($this->board);
    }
}
