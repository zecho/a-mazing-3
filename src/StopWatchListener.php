<?php
declare(strict_types=1);

namespace App;

use Symfony\Component\Console\ConsoleEvents;
use Symfony\Component\Console\Event\ConsoleCommandEvent;
use Symfony\Component\Console\Event\ConsoleTerminateEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Stopwatch\Stopwatch;

/**
 * Class StopWatchListener
 */
class StopWatchListener implements EventSubscriberInterface
{
    /**
     * @var Stopwatch
     */
    private Stopwatch $stopwatch;

    public function __construct(Stopwatch $stopwatch)
    {
        $this->stopwatch = $stopwatch;
    }

    public static function getSubscribedEvents()
    {
        return [
            ConsoleEvents::COMMAND => 'startStopWatch',
            ConsoleEvents::TERMINATE => 'stopStopWatch',
        ];
    }

    public function startStopWatch(ConsoleCommandEvent $event)
    {
        $this->stopwatch->start($event->getCommand()->getName());
    }

    public function stopStopWatch(ConsoleTerminateEvent $event)
    {
        $name = $event->getCommand()->getName();

        if (!$this->stopwatch->isStarted($name)) {
            return;
        }

        $time = $this->stopwatch->stop($name);
        $event->getOutput()->writeln(sprintf('Time: %.2fs', $time->getDuration()/1000));
    }
}
